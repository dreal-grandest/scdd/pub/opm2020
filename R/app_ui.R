#' The application User-Interface
#'
#' @param request Internal parameter for `{shiny}`.
#'     DO NOT REMOVE.
#' @import shiny
#' @importFrom shinydashboard dashboardPage dashboardHeader dashboardSidebar
#' @importFrom shinydashboard dashboardBody sidebarMenu menuItem tabItems
#' @noRd
#
# icones issues de  https://fontawesome.com/icons
#

# pour statistiques de frequentation
# Comme les deux sont executes, c'est le 2e (preprod) qui sera active.
# Pour le deploiement, utiliser de preference la partie du script "03_deploy.R" utile
if(file.exists("matomo/prod_id.R")){
      source("matomo/prod_id.R")
      message('source("matomo/prod_id.R")')
} else {
  message('matomo/prod_id.R non trouvé')
}

if(file.exists("matomo/preprod_id.R")){
      source("matomo/preprod_id.R")
  message('source("matomo/preprod_id.R")')
} else {
  message('matomo/preprod_id.R non trouvé')
}

# Options for Spinner
options(spinner.color='#6699CC')

app_ui <- function(request) {
  tagList(
    # Leave this function for adding external resources
    golem_add_external_resources(),
    # List the first level UI elements here
    dashboardPage(
      # tags$img(src = "www/logo_scdd.jpg"),
      dashboardHeader(
        title = div('Portrait Mobilit\u00e9',
                    style = "font-size: 20pt"),
        titleWidth = 250,
        # Set height of dashboardHeader


        # ---- remplacement par fichier CSS et JS ----
        # tags$head(
        tags$li(class = "dropdown",
          # Include our custom CSS
          includeCSS("inst/app/www/styles.css"),
          # includeScript("inst/app/www/matomo.js")
        ),
        # ---- fin  remplacement par CSS et JS ----
        #
        tags$li(class = "dropdown",
                tags$style(".main-header {max-height: 80px}"),
                tags$style(".main-header .navbar {max-height: 80px; padding-top: 14px;}"),
                tags$style(".main-header .logo {height: 80px; padding-top: 14px; font-size: 30pt;}"),
                tags$style(".navbar {min-height:80px !important}")
        ),
        #### ajout script JS pour Matomo ####
        # tags$head(class = "dropdown",
        tags$li(class = "dropdown",
        HTML(
          paste0(
        "<script type=\"text/javascript\">
          var _paq = _paq || [];
          _paq.push(['setDocumentTitle', 'main']);
          _paq.push(['setDownloadClasses', [\"LienTelecharg\",\"document\"]]);
          _paq.push(['trackPageView']);
          _paq.push(['enableLinkTracking']);
          (function() {
          var u=\"//audience-sites.din.developpement-durable.gouv.fr/\";
          _paq.push(['setTrackerUrl', u+'piwik.php']);
          _paq.push(['setSiteId',",matomo_site_id,"]);
          var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
          g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js';
          s.parentNode.insertBefore(g,s);
          })();
          function piwikTrackVideo(type,section,page,x1){
          _paq.push(['trackEvent', 'Video', 'Play', page]);
          }
          </script>
          <noscript><p><img src=\"//audience-sites.din.developpementdurable.gouv.fr/piwik.php?idsite=",matomo_site_id,"&rec=1&action_name='main'\"
        style=\"border:0;\" alt=\"\" /></p></noscript>
            "
            ) # paste0
          ) # HTML
        ),  # tags$li(class = "dropdown",


        #### fin ajout script JS pour Matomo ####

        # https://stackoverflow.com/questions/46961737/r-shinydashboard-textoutput-to-header
        # https://stackoverflow.com/questions/50413716/r-shiny-variable-ordered-or-unordered-list
        # https://stackoverflow.com/questions/63711442/styling-r-shiny-titlepanel-tagsa-text
        tags$li(class = "dropdown",(tags$head(
          tags$style(
            HTML(
              ".navbar {
                     display: flex;
                 }",
              ".dropdown a {
                    font-size: 22pt;
                }",
              ".dropdown img {
              position: fixed;
              top: 0;
              right: 0;
                }",
            )
          )
        ))),
        tags$li(class = "dropdown",
                tags$a(textOutput('libelle_epci2')
                )),
        tags$li(tags$img(src = "www/Dreal-Grand-Est.jpg",
                          # width = "50%", height = "50%"
                          ),
                class = "dropdown")
      ), # dashboardHeader
      dashboardSidebar(
        width = 250,
        # Adjust the sidebar
        # Source pour scroller le sidebar :
        # https://stackoverflow.com/questions/62964056/r-shiny-scrolling-sidebar-overflow
        tags$style(".left-side, .main-sidebar {padding-top: 90px;
                   overflow-y:auto; max-height: 100vh}"),
        sidebarMenu(id = "menu_tab",
                    menuItem("Accueil",
                             tabName = "choix1",
                             selected=TRUE,
                             icon = icon("map-marked")),
                    menuItem("Situation",
                             tabName = "situation",
                             icon = icon("map-marker-alt")),
                    menuItem("D\u00e9mographie",
                             tabName = "population",
                             icon = icon("users")),
                    menuItem("Revenus - in\u00e9galit\u00e9s sociales",
                             tabName = "revenus",
                             icon = icon("balance-scale")),
                    menuItem("Habitat - construction",
                             tabName = "habitat",
                             icon = icon("home")),
                    menuItem("Acc\u00e8s \u00e0 l'emploi",
                             tabName = "emploi",
                             icon = icon("walking")),
                    menuItem("Acc\u00e8s aux \u00e9tudes",
                             tabName = "etudes",
                             icon = icon("school")),
                    menuItem("Niveaux d'\u00e9quipement",
                             tabName = "equipements",
                             icon = icon("bus")),
                    menuItem("Autres ressources",
                             tabName = "tab_ressources",
                             icon = icon("globe")
                             ),
                    br(),
                    menuItem("Export et Sauvegarde",
                             tabName = "tab_export",
                             icon = icon("file-export")),
                    menuItem("Glossaire et Mentions l\u00e9gales",
                             tabName = "glossaire",
                             icon = icon("question-circle")),
                    menuItem("Nous contacter",
                             icon = icon("envelope"),
                             badgeLabel = "new",
                             href = "mailto:mobilite.st.dreal-grand-est@developpement-durable.gouv.fr?subject=[OPM] Question sur l'outil&body=posez votre question"),
                    menuItem("Journal",
                             tabName = "journal",
                             badgeLabel = "new",
                             badgeColor = "green",
                             icon = icon("newspaper")
                             ),

                    br(),
                    actionButton(label = p("Notice", br(),
                                           "utilisateur"),
                                 inputId = "telech_notice",
                                 icon = icon("book"),
                                 onclick ="window.open('https://dreal-grandest.gitlab-pages.din.developpement-durable.gouv.fr/scdd/tuto_opm/', '_blank')"
                                   )

        )  # sbMenu
      ), # dashboardSidebar
      dashboardBody(
        # source pour modifier la mise en forme de la page :
        # https://stackoverflow.com/questions/41284845/change-sidebar-menu-item-color-in-r-shiny


      # source pour fixer l'entête et le bandeau de gauche :
        # https://stackoverflow.com/questions/45706670/shiny-dashboadpage-lock-dashboardheader-on-top
        tags$script(HTML("$('body').addClass('fixed');")),

        tabItems(
          mod_selection_territoire_ui("id_selection_territoire"),
          mod_demog_ui("id_demog"),
          mod_situation_ui("id_situation"),
          mod_revenus_ui("id_revenus"),
          mod_habitat_ui("id_habitat"),
          mod_emploi_ui("id_emploi"),
          mod_etudes_ui("id_etudes"),
          mod_equipements_ui("id_equipements"),
          mod_autres_ressources_ui("id_autres_ressources"),
          mod_export_ui("id_export"),
          mod_glossaire_ui("id_glossaire"),
          mod_journal_ui("id_journal")
      ) # tabItems
      )
# ,
#       title = "OPM 2020"
    )
  )
}

#' Add external Resources to the Application
#'
#' This function is internally used to add external
#' resources inside the Shiny application.
#'
#' @import shiny
#' @importFrom golem add_resource_path activate_js favicon bundle_resources
#' @noRd
golem_add_external_resources <- function(){

  add_resource_path(
    'www', app_sys('app/www')
  )

  tags$head(
    favicon(),
    bundle_resources(
      path = app_sys('app/www'),
      app_title = 'Outil Portrait Mobilité'
      # app_title = 'opm2024'
    )
    # Add here other external resources
    # for example, you can add shinyalert::useShinyalert()
  )
}

