#' import_routes_osm
#' rcupere les routes circulables depuis OpenStreetMap.
#' @param x objet avec composante spatiale, representant un territoire, en lambert 93.
#'
#' @return un objet spatial projete en lambert 93, avec les principales routes du territoire
#' (lignes ou polygones), caracterisees par un highway, factor, avec les valeurs ordonnees :
#' c("motorway", "trunk", "primary", "secondary", "tertiary", "residential", "unclassified")
#' @export
#' @importFrom sf st_bbox st_transform st_intersection st_crs<-
#' @importFrom dplyr select filter mutate bind_rows
#' @importFrom osmdata opq add_osm_feature osmdata_sf
import_routes_osm <- function(x){

  # On recupere les limites extremes de notre jeu de donnees spatiales (extremites nord, sud, est, ouest), en coordonnees polaires.
  bb_epci <- sf::st_bbox(x %>%  st_transform(crs = 4326))
  # on va requeter OpenStreetMap sur ces memes limites
  fond_osm_highway <- opq(bbox = bb_epci) %>%
    # on recupere toutes les donnees liees aux chemins (highway), des autoroutes aux petites routes residentielles, ainsi que les echangeurs ou "sorties".
    add_osm_feature(key = 'highway', value = c("motorway", "motorway_link", "trunk", "primary",
                                               "secondary", "tertiary", "residential",
                                               "unclassified", "trunk_link",
                                               "primary_link", "tertiary_link")
    ) %>%
    # on recupe un objet de format osmdata contenant les donnees sous format sf.
    osmdata_sf()

  # L'objet osmdata est une liste. Dans cette liste, on retrouve un objet sf (dataframe spatial) par type d'objet (points, lines, polygons, multilines, multipolygons)
  # on les rassemble en un seul dataframe. Ici les routes sont soit des lines (cas general) soit des polygons (echangeurs).
  # on reprojette en lambert 93 (crs 2154) et on ne garde que le type de la route (variable highway)
  carte_osm_highway <- bind_rows(
    fond_osm_highway$osm_lines %>% st_transform(crs = 2154) %>% select(osm_id, highway),
    fond_osm_highway$osm_polygons %>% st_transform(crs = 2154) %>% select(osm_id, highway)
  ) %>%
    # On transforme le type  de la route en factor
    mutate(highway = highway %>% factor(levels = c("motorway", "trunk", "primary",
                                                   "secondary", "tertiary", "residential",
                                                   "unclassified"))) %>%
    # on retire toutes les routes sans type
    filter(!is.na(highway))

  # on impose de nouveau le crs a 2154 (st_transform ne semble pas suffire)
  st_crs(carte_osm_highway) <- 2154
    # on ne garde que les routes de notre objet geographique de depart.
  carte_osm_highway %>%  st_intersection(x)

}




#' affiche_carte_covoiturage
#'
#' @param x objet geographique au format sf, donnant des points. et possedant la colonne `nom_lieu`
#' @param carte objet geographique au format sf contenant les territoires sur lesquels on souhaite afficher les lieux de covoiturage.
#' @param osm booleen indiquant si l'on souhaite charger et afficher les donnees OpenStreetMap du reseau routier
#' @param titre une chaine de caractere correspondant au titre de la carte
#' @param labelsource chaine de caracteres indiquant la source de la donnee. Prevue pour les donnees de lieux de covoiturage la valeur par defaut est "transport.data.gouv.fr"
#' @param ndl chaine de caracteres correspondant a une note de lecture/de bas de page pour le tableau. Par defaut, la chaine est vide.
#'
#'
#' @return une carte au format ggplot, sur le territoire choisi rempli de vert pale, avec les lieux de covoiturage representes par des losanges bleu fonce et le nom du lieu en bleu fonce. Selon le parametre osm, la carte rpresente aussi les routes du terrtoire selon le code suivant :
#' - autoroutes : rouge fonce, epaisse
#' - 4 voies : orange fonce, legerement moins epaisse
#' - nationale : rouge, taille moyenne
#' - departementales : jaune, legerement plus fine
#' - communales : blanche, encore un peu plus fine
#' - residentielles et autres : gris transparent, fine.
#' @export
#' @importFrom ggplot2 ggplot geom_sf aes scale_colour_manual scale_size_manual labs guides theme_void annotate
#' @importFrom scales alpha
#' @importFrom ggrepel geom_text_repel
#' @importFrom glue glue
#' @importFrom sf st_coordinates  st_centroid st_union
affiche_carte_covoiturage <- function(x,
                                      carte,
                                      osm = FALSE,
                                      titre = "Aires de covoiturage",
                                      labelsource = "transport.data.gouv.fr",
                                      ndl = "") {

  # on trace la base de notre carte : l'objet geographique carte passe en parametre, rempli de vert pale
  carte_finale <- ggplot() +
    geom_sf(data = carte, fill = alpha("#FAEB00", 0.1))  +
    # on ne met pas e theme , sans axe, sans grille, fond blanc.
    theme_void() +
    labs(
      # On definit le Titre de la carte et sa note de lecture
      title = titre,
      caption = glue("Source : {labelsource}\n{ndl}")
    )


  if (osm){
    # Si le parametre osm vaut TRUE, on charge les routes disponibles sur osm et on les trace sur notre base
    carte_finale <- carte_finale +
      # on importe les donnees osm
      geom_sf(data = import_routes_osm(carte),
              # on fera varier la couleur et l'epaisseur des routes selon leur type
              aes(colour = highway, size = highway)) +
      # on definit manuellement la couleur, puis l'epaisseur de chaque type de route.
      scale_colour_manual(values = c("motorway" = "darkred",
                                     "trunk" = "darkorange",
                                     "primary" = "red",
                                     "secondary" = "gold",
                                     "tertiary" = "white",
                                     # alpha permet de rendre ces couleurs transparantes
                                     "residential" = alpha("grey",.8),
                                     "unclassified" = alpha("grey",.8))) +
      scale_size_manual(values = c("motorway" = 2,
                                   "trunk" = 1.7,
                                   "primary" = 1.1,
                                   "secondary" = .9,
                                   "tertiary" = .7,
                                   "residential" = .5,
                                   "unclassified" = .5))
  }
if(nrow(x) > 0) {
  carte_finale +
    # on ajoute les aires de covoiturage
    geom_sf(data = x,
            # en bleu fonce
            fill = "darkblue",
            # losanges
            shape = 23)+
    # on ajoute le nom du lieu de covoiturage. cette fonction permet d'empecher le chevauchement des noms si possible
    geom_text_repel(
      data = x,
      aes(label = nom_lieu, geometry = geometry),
      # je ne sais plus a quoi sert ce parametre
      stat = "sf_coordinates",
      # couleur : bleu fonce
      colour = "darkblue",
      # texte en gras
      fontface = "bold",
      # taille du text : 3pt
      size = 3
    )   +
    # on n'affiche pas de legende
    guides(fill = "none", colour = FALSE, size = FALSE)
} else {
  centre <- st_coordinates(st_centroid(st_union(carte)))

  carte_finale +
    annotate("text", x = centre[1],
             y = centre[2],
             label = "Pas d'aires \nde covoiturage",
             hjust=0.5, vjust=0,
             col="blue", cex=6,
             fontface = "bold", alpha = 1)
  }
}
