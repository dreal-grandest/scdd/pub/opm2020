
#' Fonction ote_accents : permet de supprimer les caracteres accentues d'une chaine de caracteres
#' Source : https://data.hypotheses.org/564
#' @param text une chaine de caracteres
#'
#' @return une chaine de caracteres debarassee de ses caracteres accentues
#' @export
ote_accents <- function(text) {
  text <- gsub("['`^~\"]", " ", text)
  text <- iconv(text, from = "UTF-8", to="ASCII//TRANSLIT//IGNORE")
  text <- gsub("['`^~,\"]", "", text)
  text <- gsub(" ", "_", text)
  return(text)
}
