#' Fonction d'agregation des donnees demographiques
#'
#' @param x le tableau des donnees a l'echelle communale
#'
#' @return le tableau des donnees agregees selon 3 niveaux : EPCI, departemental et regional
#' @export
#' @importFrom dplyr bind_rows everything filter group_by mutate rename select summarise_if ungroup
#' @importFrom forcats fct_inorder
#' @importFrom stringr str_c
#'
#'
#' @examples
#' library(readr)
#'d_pop_r44_file <- system.file("donnees_rds/d_pop_r44.rds",
#'                                package = "opm2020")
#'d_demog_global <- read_rds(d_pop_r44_file)
#'d_demog_agreg <- d_demog_global %>% agrege_donnees_demog()


agrege_donnees_demog <- function(x) {
  bind_rows(x %>%
              rename(code_perimetre = code_epci) %>%
              group_by(code_perimetre) %>%
              summarise_if(is.numeric,
                           sum, na.rm = T) %>%
              ungroup() %>%
              mutate(type_perimetre = "EPCI",
                     lib_perimetre = "EPCI")
            ,
            x %>%
              rename(code_perimetre = code_dep) %>%
              group_by(code_perimetre) %>%
              summarise_if(is.numeric,
                           sum, na.rm = T) %>%
              ungroup() %>%
              mutate(type_perimetre = "DEP",
                     lib_perimetre = "Departement")
            ,
            x %>%
              rename(code_perimetre = code_reg) %>%
              # filter(code_perimetre == "44") %>%
              group_by(code_perimetre) %>%
              summarise_if(is.numeric,
                           sum, na.rm = T) %>%
              ungroup() %>%
              mutate(code_perimetre = str_c("R",code_perimetre),
                     type_perimetre = "REG",
                     lib_perimetre = "Region")
  ) %>%
    mutate(type_perimetre = fct_inorder(type_perimetre),
           code_perimetre = fct_inorder(code_perimetre),
           lib_perimetre = fct_inorder(lib_perimetre)) %>%
    select(type_perimetre, code_perimetre, lib_perimetre,
           everything())
}
