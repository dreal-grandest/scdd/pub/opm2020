library(testthat)
library(readr)
library(dplyr)

d_men_r44_file <- system.file("donnees_rds/d_menages_r44.rds",
                              package = "opm2020")
d_menages <- read_rds(d_men_r44_file)


test_that("is a dataframe", {
  expect_true(
    is.data.frame(d_menages  %>%
                    prepare_donnees_equipement_auto())
)
})
