library(testthat)
library(readr)
library(dplyr)

# [X] Utiliser l’exemple reproductible

d_pop_r44_file <- system.file("donnees_rds/d_pop_r44.rds",
                              package = "opm2020")
d_demog_global <- read_rds(d_pop_r44_file)

d_demog_agreg <- d_demog_global %>% agrege_donnees_demog()
d_demog_logement <- d_demog_agreg %>% prepare_indicateurs_logement()

tableau_residences <- d_demog_logement %>% affiche_tableau_residences()
tableau_logements_vacants <- d_demog_logement %>% affiche_tableau_logements_vacants()

test_that("donnees preparees pour indicateurs logement est un dataframe", {
  expect_true(
    is.data.frame(d_demog_logement)
  )
})


test_that("tableau residences principales/secondaires is kable", {
  expect_true(is.character(tableau_residences))
  expect_equal(class(tableau_residences), c("kableExtra","knitr_kable"))
})

test_that("tableau logements vacants is kable", {
  expect_true(is.character(tableau_logements_vacants))
  expect_equal(class(tableau_logements_vacants), c("kableExtra","knitr_kable"))
})
