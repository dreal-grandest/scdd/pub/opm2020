library(testthat)
library(readr)
library(dplyr)


test_that("affiche_carte_region produit une carte", {

  # EPCIs de test
  code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)

c_dep <- read_rds(system.file("donnees_rds/carte_dep_r44.rds",
                                package = "opm2020"))
c_epci <- read_rds(system.file("donnees_rds/carte_epci_r44.rds",
                                 package = "opm2020"))

  c_region <-
    affiche_carte_region(code_epci_test,
                         c_dep,
                         c_epci)

  expect_type(c_region, "list")
  expect_equal(class(c_region), c("gg", "ggplot"))
})
