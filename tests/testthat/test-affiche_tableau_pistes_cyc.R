library(readr)
library(dplyr)
library(kableExtra)
library(testthat)

d_pistes_cyc <-
  read_rds( system.file("donnees_rds/d_pistes_cyc_agreg_r44.rds",
                        package = "opm2020"))

# EPCI test
code_epci_test <- "240800847"  # CC des Portes du Luxembourg

tableau_pistes_cyc <- d_pistes_cyc %>%
  filter(code_epci == code_epci_test) %>%
  affiche_tableau_pistes_cyc()

test_that("tableau is a kable", {
  expect_equal(class(tableau_pistes_cyc),
               c("kableExtra","knitr_kable"))
})
