library(testthat)
library(readr)
library(dplyr)


test_that("affiche_carte_communes produit une carte", {

  # EPCIs de test
  code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)

  c_com <- read_rds(system.file("donnees_rds/carte_communes_r44.rds",
                                package = "opm2020")) %>%
    filter(code_epci == code_epci_test)

  c_region <-
    affiche_carte_communes(c_com)

  expect_type(c_region, "list")
  expect_equal(class(c_region), c("gg", "ggplot"))
})
