library(testthat)
library(readr)
library(dplyr)
library(ggplot2)

d_pop_r44_file <- system.file("donnees_rds/d_pop_r44.rds",
                              package = "opm2020")
d_demog_global <- read_rds(d_pop_r44_file)

code_epci_test <- "240800847"  # CC des Portes du Luxembourg

test_that("is output a ggplot (in fact, a list)", {
  expect_type(
    d_demog_global%>%
      agrege_donnees_demog() %>%
      affiche_pyramide_ages(code_epci_test,
                            labelsource = "Insee RP 2016",
                            ndl = "commentaire test")
    ,
    "list"
  )
})
