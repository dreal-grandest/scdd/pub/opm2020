## <span style="color: rgb(102,153,204);">Aide à la lecture et définitions</span> 
<hr/>

### Indice base 100
L'indice d'une grandeur est le rapport entre la valeur de cette grandeur au cours d'une période courante et sa valeur au cours d'une période de base. Il mesure la variation relative de la valeur entre la période de base et la période courante. Souvent, on multiplie le rapport par 100 ; on dit : indice base 100 à telle période.
Les indices permettent de calculer et de comparer facilement les évolutions de plusieurs grandeurs entre deux périodes données.
(Source : INSEE)

### Taux de vacance

Le taux de vacance est le rapport entre le nombre de logements vacants  et le nombre total de logements du parc. Filocom surestime généralement la vacance des logements par rapport aux données INSEE, par suite d’une prise en compte de logements vides qui ne pourraient plus être proposés à la location.

### Densité résidentielle

La densité résidentielle est le rapport entre le nombre de logements total et la surface cadastrée des parcelles construites à l’instant t.

### Indice de concentration de l'emploi

L'indice de concentration de l'emploi mesure le rapport entre le nombre d'emplois total d'un territoire sur le nombre de résidents qui ont un emploi. Le nombre d'emplois sur un territoire est issu de l'information du lieu d'emploi du rencensement de la population. C'est pourquoi, les emplois d' étrangers travaillant en France sont hors champ (voir 2.4 premier paragraphe de la <a href="https://www.insee.fr/fr/statistiques/fichier/2383177/fiche-activite-emploi-chomage_2020-06-29.pdf">documentation de l'Insee sur le recensement de la population thématique emploi</a>),

### Gamme d'équipements

Pour regrouper les équipements et services selon leur fréquence sur le territoire, l’Insee a défini trois gammes. La gamme de proximité regroupe les équipements les plus fréquents (école élémentaire, boulangerie...), la gamme supérieure les moins fréquents (hôpital, lycée...) ; la gamme intermédiaire regroupe les équipements dont la fréquence se situe entre ces deux pôles (collèges, supermarchés...).

### Panier des équipements

Les trois gammes regroupent en tout 102 équipements, sans tenir compte de leur importance pour la population. Pour affiner cette approche, le Commissariat Général à l’Égalité des Territoires a sélectionné un **panier de 22 équipements et services de la vie courante**, en collaboration avec l’Insee :
école élémentaire, bureau de poste et assimilé, médecin, station-service, épicerie-supérette, supermarché, banques-caisses d’épargne, écoles de conduite, police-gendarmerie, librairie-papeterie, collège, école maternelle, chirurgien-dentiste, infirmier, pharmacie, laboratoire d’analyses médicales, service d’aide aux personnes âgées, garde d’enfants d’âge préscolaire, salle ou terrain multisports, boulangerie, salon de coiffure, cafés-restaurants.  
La composition du panier est retrouvable dans la figure complementaire 2 dans les donnees telechargeables accompagnant l'Insee Premiere du 06/01/2016 :
https://www.insee.fr/fr/statistiques/1908098#documentation

### Mobilité professionnelle et Mobilité scolaire

#### Déplacements frontaliers

- Les données sont issues du recensement de la population. Les personnes non-résidentes en France, mais travaillant ou étudiant en France n'ont pas été interrogées et sont donc exclues du champ.  
- La codification des communes frontalieres est etablie selon une nomenclature interne a l'INSEE. L'utilisation de la nommenclature NUTS permet de rattacher ces communes a un niveau supra-communal.  
- Quelques communes d'Allemagne et de Suisse n'ont pas d'equivalent dans la table de passage NUTS. Monaco n'a pas non plus d'equivalent. Ces communes ne sont donc pas rattachees a un territoire equivalent EPCI.

#### Calcul de distance

Les distances sont calculées entre le centre des deux communes concernées par le flux (lieu de domicile et lieu de travail/lieu d'étude). Par conséquent, tous les flux hors de France n'ont pas été pris en compte pour les indicateurs indiquant une classe de distance.


#### Quelques flux écartés

De l'ordre de quelques unités et concernant des communes hors du Grand Est :  

- Quelques communes-iles et quelques communes des Collectivites d'outre-mer ne sont pas rattachées à un EPCI.


#### Mode de transport

Pour connaître l'évolution récente du recensement du mode de transport pour les déplacements-domicile travail, voir <a href=https://www.insee.fr/fr/statistiques/fichier/2383177/fiche-depl-dom-travail_2020-06-29.pdf> la documentation afférente de l'Insee</a> page 5-6.


### Lieux de covoiturage

la DREAL Grand Est utilise les dernières versions des jeux de données communiqués par transport.data.gouv.fr  
Ces données sont disponibles sur transport.data.gouv.fr sous la licence « Open Database Licence » (ODbL).
  
  La documentation complète de ces données est disponible ici :
  https://doc.transport.data.gouv.fr/producteurs/points-de-rencontre-de-covoiturage  
  Pour faire apparaître les lieux de covoiturage de votre commune, vous y trouverez une procédure à suivre.
  
### Données pistes cyclables  

Les données pistes cyclables sont extraites du site transport.data.gouv.fr, La documentation complète de ces données est disponible ici : 

<a href="https://doc.transport.data.gouv.fr/producteurs/amenagements-cyclables/schema-au-format-tableur " target="_blank">Schéma des aménagements cyclables</a>

 

Une série de fiches pratiques du Cerema concernant les aménagements en faveur du vélo sont disponibles ici :  

<a href="https://www.cerema.fr/fr/actualites/amenagements-faveur-du-velo-serie-fiches-pratiques-du-cerema" target="_blank">Fiches pratiques</a>

ainsi qu'une boite à outils des aménagements cyclables :  

<a href="https://www.cerema.fr/system/files/documents/2019/07/7_1_boiteoutil_velo_cotitaaix_280519.pdf" target="_blank">Boite à outils (PDF)</a>


Il y a deux types d'aménagement pour chacun des tronçons cyclables : voie de gauche et voie de droite. Les typologies sont nombreuses et parfois différentes entre le côté gauche et droit. Un choix a été fait pour ne garder que des grandes familles de typologies :
* La typologie "Piste cyclable" a été choisie quand le côté droit ou gauche du tronçon était une piste cyclable.
* La typologie "Bande cyclable" a été choisie quand le côté droit ou gauche du tronçon était une bande cyclable.
* La typologie "Voie verte" a été choisie quand le côté droit ou gauche du tronçon était une voie verte.
* La typologie "Couloir bus+vélo" a été choisie quand le côté droit ou gauche du tronçon était un couloir bus+vélo.
* Dans tous les autres cas, la typologie "Autre" a été choisie.


