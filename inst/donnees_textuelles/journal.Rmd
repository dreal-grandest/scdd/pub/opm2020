## <span style="color: rgb(102,153,204);">Journal</span> 
Évolutions de l'application et des données qu'elle valorise.
<hr/>

### Mise a jour
Dernière mise a jour : 04/12/2024

### OPM 2024

##### Publiée le 16/09/2024

#### Évolution des données

-   Mise à jour des **données parkings** dans le module "Accès aux services",  
-   Mise à jour des **données IRVE** (bornes de recharge) dans le module "Accès aux services",  
-   Mise à jour des **données pistes cyclables** dans le module "Accès aux services",  
-   Mise à jour des **données gares** dans le module "Accès aux services",  
-   Mise à jour des données **aires covoiturage** dans le module "Accès aux services".  

#### Évolutions de l'application

-   ajout d'une fonctionnalité d'export des données pour l'EPCI concerné en format XLSX,  
-   modification des mentions légales.  

### OPM 2023

##### Publiée le 17/10/2023

#### Évolution des données

-   agrégation des données selon COG 2023 , incluant le nouvel EPCI (CC Gérardmer Hautes Vosges)
-   mise à jour des données au millésime le plus récent (2020 à 2023 selon les sources), à l'exception des données navettes domicile/travail et domicile études, inchangées.


#### Évolutions de l'application

- notice utilisateur : au format HTML, hébergée sur une page Gitlab, avec mise à jour facilitée,
- modification du lien vers les données de trafic routier **CoMPTAGE**.  

##### Publiée le 29/07/2023

#### Évolution des données

-   mise à jour des **données parkings** dans le module "Accès aux services"
-   mise à jour des **données IRVE** (bornes de recharge) dans le module "Accès aux services"
-   mise à jour des **données pistes cyclables** dans le module "Accès aux services"


#### Évolutions de l'application

-   Simplification de la procédure d'export sous forme Html : un seul bouton au lieu de 2 pour préparer et exporter le portrait  
-   cartes de localisation de l'EPCI et des communes dans le portrait Html : conversion en carte plotly, plus rapide à exporter  

### OPM 2022

##### Publiée le 3 septembre 2022

#### Évolution des données

-   Ajout de **données parkings** dans le module "Accès aux services"
-   Ajout de **données IRVE** (bornes de recharge) dans le module "Accès aux services"
-   Ajout de **données pistes cyclables** dans le module "Accès aux services"
-   Ajout de **données gares** dans le module "Accès aux services"


#### Évolutions de l'application

-   **Refonte graphique** de l'OPM
-   Ajout d'un **message de bienvenue**, présentant l'application
-   Ajout d'un bouton **"Nous contacter"** dans le bandeau de gauche
-   Ajout d'un bouton **"Journal"** dans le bandeau de gauche
-   Ajout d'un **message d'alerte** si l'utilisateur ne choisit pas de territoire d'étude
-   Possibilité de **sauvegarde d'une session** (commentaires personnalisés)

### OPM 2020

#### Caractéristiques de l'application :  

-  choix d'un territoire du Grand Est (département puis EPCI) par listes déroulantes, visualisé sur carte dynamique,
-  affichage de **27 indicateurs** (graphiques, cartes, tableaux), classés selon **5 thématiques** (démographie, revenus et niveau de vie, habitat, accès à l'emploi, accès à divers services) 
-  export du portrait complet sous forme d'un fichier HTML,  
-  possibilité d'ajout de *commentaires personnalisés* pour chaque thématique , qui seront ajoutés au portrait final,  
-  notice d'utilisation en PDF téléchargeable.  


