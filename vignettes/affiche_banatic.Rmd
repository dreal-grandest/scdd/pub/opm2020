---
title: "affiche_banatic"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_banatic}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
editor_options: 
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


# Objectif
Pour un EPCI donne, afficher ses principales caracteristiques, issues de la base de donnees Banatic :  

- [x] Date de creation de l'EPCI
- [x] Date d'effet de la creation de l'EPCI
- [x] Presidence de l'EPCI (civilite, Prenom, NOM)
- [x] Adresse du siege (3 champs possible + le code postal et le nom de la commune)
- [x] n° de telephone.


# Prealables
- [x] recuperer les donnees issues de Banatic, disponibles a l'echelle communale de l'EPCI
- [x] retenir uniquement les donnees concernant le territoire d'interet : l'EPCI selectionne
- [x] retenir uniquement les caracteristiques que l'on souhaite afficher.

# A effectuer
- [x] afficher les caracteristiques en explicitant pour chacune quelle caracteristique on affiche 



# Chargement des donnees/packages

```{r setup}
library(dplyr)
library(tidyr)
library(readr)
library(opm2020)
```

# Lecture des donnees
recuperer les donnees issues de Banatic, disponibles a l'echelle communale de l'EPCI

```{r }

# Chemins vers les fichiers du package opm2020
l_coord_banatic_file <- system.file("donnees_rds/l_coord_banatic.rds",
                              package = "opm2020")

l_coord_banatic <- read_rds(l_coord_banatic_file)

code_epci_test <- c("240800847")  # CC des Portes du Luxembourg, (Dep 08)

```

# Preparation des donnees
Retenir uniquement les donnees concernant le territoire d'interet : l'EPCI selectionne.  
Retenir uniquement les caracteristiques que l'on souhaite afficher.
```{r}

carac_banatic <- l_coord_banatic  %>% 
    filter(code_epci %in% code_epci_test) %>% 
    select(date_de_creation, date_deffet, 
           civilite_president, prenom_president, nom_president, 
           adresse_du_siege_1, adresse_du_siege_2, adresse_du_siege_3, code_postal_nom_siege,
           telephone_du_siege, courriel_du_siege, site_internet)

```



## Affichage du texte

```{r}
affiche_banatic(carac_banatic)
```

# Utilisation

`markdown::renderMarkdown(text = affiche_banatic(carac_banatic))`

