---
title: "affiche_tableau_parkings"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_tableau_parkings}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(opm2020)
library(dplyr)
library(kableExtra)
library(tidyr)
library(readr)
library(sf)
library(glue)
```

Cette fonction permet l'affichage des données de localisation des parkings des communes d'un EPCI en format tidy long.

Elle distingue les différentes catégories de parkings :
- Aire de covoiturage  
- Parking couvert  
- Parking relais  
- Parking souterrain  
- Parking touristique isolé  
- Autre (colonne NAT_DETAIL non renseignée)

Elle permet  egalement l'affichage de la source des donnees.

#### A. Chargement des données  ####

```{r}

c_parking <- read_rds(system.file("donnees_rds/c_parking.rds",
                              package = "opm2020"))
```

#### B. Choix EPCI ####

```{r}
code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)
# code_epci_test <- "246700488" # EMS
# code_epci_test <- "200039865"   # Metz
# code_epci_test <- "200067213"   # Reims
# code_epci_test <- "200013050"   # 
# 

```



#### C. Utilisation fonction ####


```{r, warning=FALSE}
c_parking %>%
  filter(code_epci == code_epci_test) %>% 
  affiche_tableau_parkings(agrege_epci = TRUE,
                           labelsource = "IGN 2022",
                           ndl = "Il est possible que tous les types de parkings ne soient pas correctement renseign\u00e9s")

```

