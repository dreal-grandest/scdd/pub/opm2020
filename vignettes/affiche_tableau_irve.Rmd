---
title: "affiche_tableau_irve"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_tableau_irve}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(opm2020)
library(dplyr)
library(kableExtra)
library(readr)
library(sf)
library(glue)
```

Cette fonction permet l'affichage des données de localisation des IRVE d'un EPCI, par classe de puissance.


#### A. Chargement des données  ####

```{r}
# d_irve <- read_rds("~/R_stats/opm2020/inst/donnees_rds/d_irve_epci_r44.rds")

d_irve <- read_rds(system.file("donnees_rds/d_irve_epci_r44.rds",
                              package = "opm2020"))
```

#### B. Choix EPCI ####

```{r}
# code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)
# code_epci_test <- "246700488" # EMS
# code_epci_test <- "200039865"   # Metz
# code_epci_test <- "200067213"   # Reims
code_epci_test <- "200067874"   #
# 

```


## Utilisation de la fonction

```{r}
d_irve %>%
  filter(code_epci == code_epci_test) %>% 
  # select(-code_epci) %>% 
  affiche_tableau_irve(labelsource = "transport.data.gouv",
                           ndl = "Il est possible que toutes les stations ne soient pas correctement renseign\u00e9s.
Pour compléter les données, des informations sont disponibles sur le site transport.data.gouv.fr :
https://transport.data.gouv.fr/infos_producteurs")

```

