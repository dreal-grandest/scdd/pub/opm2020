---
title: "affiche_carte_covoiturage"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_carte_covoiturage}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
editor_options: 
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


# Objectif  
Repérer les aires de covoiturage connues sur l'EPCI, sur une carte explicite proposant egalement une visualisation des principales routes.

# Prealables
- recuperer les donnees consolidees des aires de covoiturage, issues de transport.data.gouv  
- retenir uniquement les donnees concernant l'EPCI selectionne  
  
# A effectuer

- [X] afficher les points sur un fond de carte adapte (les communes de l'EPCI)
- [X] charger des donnes OpenStreetMap (OSM) pour etoffer la carte
- [X] utiliser la fonction -> afficher la carte sur EPCI test

# Chargement des donnees/packages

```{r setup, message=FALSE}
library(opm2020)
library(readr)
library(dplyr)
library(sf)

options(OutDec = ",")  # passage en virgule tout au debut du script

# donnees des aires de covoiturage
carte_lieux_covoiturage <- read_rds( system.file("donnees_rds/carte_lieux_covoiturage.rds",
                                        package = "opm2020"))

# st_crs(carte_lieux_covoiturage) <- 2154

# carte des communes
carte_communes_r44 <- read_rds(system.file("donnees_rds/carte_communes_r44.rds",
                                   package = "opm2020"))

# st_crs(carte_communes_r44) <- 2154 # déconseillé
carte_communes_r44 <- st_transform(carte_communes_r44 , crs = 2154)
# EPCIs de test
# code_epci_test <- "200013050"  # Outre Forêt (67)
# code_epci_test <- "246700777" # val de Villé
# code_epci_test <- "200039865" # Metz Metropole
code_epci_test <- "200000545" # Romilly (zéro aire de covoit)


```


# Filtre des donnees
On ne garde que les communes et les lieux de covoiturage de l'EPCI
```{r}
# On garde uniquement les contours des communes de l'EPCI choisi
 carte_communes_choisies_lambert <- carte_communes_r44  %>%
        filter(code_epci == code_epci_test)

 
 # on ne garde que les aires de covoiturage de l'EPCI (intersectant les communes de l'EPCI)
carte_lieux_covoiturage_epci <- carte_lieux_covoiturage %>%
  st_intersection(carte_communes_choisies_lambert)


```


# OpenStreetMap

OSM delivre :

- des fonds de cartes (images goreferencees) raster, faits pour etre regarde avec une certaine resolution (il faut bien choisir son niveau de zoom a importer)  
  - {rosm} (OSM, raster, cache)  
  - {ceramic} (mapdeck (API, compte), raster, cache)  
- des vecteurs : des lignes, des points  
  - {osmdata}: https://docs.ropensci.org/osmdata  

l'API d'OSM qui est utilisee par ces package, prend en entree des coordonnees geographiques (WGS, crs = 4326).
L'API retourne des donnees dans la projetction Mercator (autre projection, mondiale mais peu pratique). Il faut alors reprojeter le resultat.

Si on souhaite un fond raster, il est preferable de preparer les cartes en amont et de les importer dans l'application : operation longue et couteuse. alourdira de plusieurs dizaines de Mo l'appli.

__Preferons des vecteurs__

On utilisera ici le package `osmdata`.  
On recupere toutes les routes (de l'autoroute a la petite route ou route residentielle).

```{r}
options(OutDec = ".")  # passage en point


carte_osm_highway <- import_routes_osm(carte_communes_choisies_lambert)

options(OutDec = ",")  # passage en virgule 

```


# Affichage de la carte

On fait le choix de proposer d'afficher les lieux de covoiturage avec ou sans les routes issues de OSM, pour ne pas surcharger le temps de chargement de l'onglet dans l'appli. L'utilisateur pourra choisir avec un bouton d'afficher les routes et de les faire figurer dans le portrait final telecharge.


```{r, fig.width=8, fig.height= 6, out.width="90%"}

options(OutDec = ".")  # passage en point

affiche_carte_covoiturage(carte_lieux_covoiturage_epci, 
                          carte_communes_choisies_lambert,
                          osm = FALSE)
  

affiche_carte_covoiturage(carte_lieux_covoiturage_epci, 
                          carte_communes_choisies_lambert,
                          osm = TRUE)


  

```


# Essais

## cartography

Le package `cartography` permet de charger un fond raster d'OSM a partir d'un objet sf  et de l'adapter automatiqument (perimetre et niveau de zoom) a notre objet.  
La sortie n'est pas utilisable avec ggplot2 : ne "maitrisant" que plot et ggplot2, les cartes sont moins esthetiques avec plot et non stockables dans un objet R, on fait alors le choix de ne pas utiliser ce package.

```{r, fig.width=8, fig.height= 6, out.width="90%", eval=FALSE}
library(cartography)



# on telecharge le fond OpenStreetMap sur les limites de l'EPCI
communes_choisies_osm_lambert <- getTiles(carte_communes_choisies_lambert,
                                          type = 'OpenStreetMap',
                                          # je ne sais pas ce qu'implique ce parametre, mais la carte est plus adaptee
                                          crop = TRUE)

# On trace le fond OSM
tilesLayer(communes_choisies_osm_lambert)
# on rajoute le contour des communes
plot(st_geometry(carte_communes_choisies_lambert),
     add = TRUE)
if(nrow(carte_lieux_covoiturage_epci) > 0) {
# On ajoute les point localisant les aires de covoiturage
plot(st_geometry(carte_lieux_covoiturage_epci), 
     pch = 25,
     add = TRUE)
# on ajoute le nom de l'aire de covoiturage
 text(x=st_coordinates(carte_lieux_covoiturage_epci)[,"X"], # Coordonnée des points auxquels on va ajouter un text
        y=st_coordinates(carte_lieux_covoiturage_epci)[,"Y"],
        labels = carte_lieux_covoiturage_epci$nom_lieu, # texte que l'on va afficher
        pos=1, # on affiche le texte sous le point (2 = à gauche,...)
        cex = 0.75, # taille du texte
        col = "darkblue", #couleur du texte
        font = 2 # en gras (3 = italic, 4 = gras italic)
   )
}
```


## OpenStreetMap

Package `OpenStreetMap`.  
la fonction openmap permet de charger un fond raster d'OpenStreetMap.  
utilisable ensuite avec ggplot, en faisant `autoplot(objet_genere_avec_openmap) +` ou avec le package `cartomisc`  : https://statnmap.github.io/cartomisc/

impossible de charger le package OpenStreetMap, necessite JAVA_HOME (??) :

 .onLoad a échoué dans loadNamespace() pour 'rJava', détails :   
  appel : fun(libname, pkgname)  
  erreur : JAVA_HOME cannot be determined from the Registry 
