---
title: "affiche_carte_densite_resid"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{affiche_carte_densite_resid}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
editor_options: 
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


# Objectif  
Afficher la densite résidentielle (en logement/hectare) sous forme d'une carte chloroplethe a l'echelle des communes de l'EPCI choisi.

# Prealables
- recuperer les donnees issues des fichiers fonciers, disponibles a l'echelle communale
- retenir uniquement les donnees concernant l'EPCI selectionne
- retenir les variables d'interet (nombre de logement, superficie, perimetre)
- potentiellement calculer les densites de population (avec la formule _densite = nombre de logement / superficie totale des parcelles en m^2 * 10000_)
  
# A effectuer

- [X] ouvrir les donnees et la carte
- [X] selectionner les donnees (avec millesime le plus recent)
- [X] creer des classes de densite (echelle du Grand Est)
- [X] associer donnees carto et attributaires (merge)
- [X] creer la fonction d'affichage et l'extraire dans un script R
- [X] utiliser la fonction -> afficher la carte sur EPCI test

# Chargement des donnees/packages

```{r setup, message=FALSE}
library(opm2020)
library(readr)
library(dplyr)
library(ggplot2)
library(sf)

# donnees demographiques
d_densite_resid <- read_rds( system.file("donnees_rds/d_densite_resid.rds",
                                        package = "opm2020"))

# carte des communes
c_communes <- read_rds(system.file("donnees_rds/carte_communes_r44.rds",
                                   package = "opm2020"))
# carte des communes
l_epcicom_fr <- read_rds(system.file("donnees_rds/l_epcicom_fr.rds",
                                   package = "opm2020"))

# EPCIs de test
# code_epci_test <- "240800847"  # CC des Portes du Luxembourg, (Dep 08)
code_epci_test <- "246700777" # val de Villé  
```




# Preparation des donnees

## classes de densite
Determination des classes de densite pour la carte. Les classes sont determinees pour l'ensemble des donnees Grand Est

```{r, eval=FALSE}
d_densite_resid %>%
  pull(densite) %>%
  quantile(probs = seq(0, 1, 0.1))
```

A partir des quantiles calcules ci-dessus, determination d'un decoupage equilibre avec des valeurs entieres et coherentes.  
Ce seront les valeurs utilisees dans l'application :
```{r}
# classes de densite residentielle
classes_densresid_6 <- c(0, 5, 7, 10, 15, Inf)
labs_classes_densresid_6 <- c("<5", "]5-7]","]7-10]","]10-15]",">15")


```


## filtre sur le perimetre choisi
```{r}
# carte des communes de l'EPCI selectionne
c_communes_epci_choisi <- c_communes %>% 
  filter(code_epci == code_epci_test)
l_epcicom_choisi <- l_epcicom_fr %>% 
  filter(code_epci == code_epci_test)

# On filtre les donnees sur les communes de l'EPCi d'etude.
# On repartit les communes dans les differntes classes de densite crees precedemment.
d_densite_resid_filter <-
  d_densite_resid %>% 
  filter(code_epci == code_epci_test ) %>% 
  mutate(dens_classes = cut(densite,
                            labels = labs_classes_densresid_6,
                            breaks = classes_densresid_6))
```


```{r}


# Association des données attributaires
# pour que le merge() fonctionne , il faut d'abord la carte (objet sf) comme premier paramètre
c_communes_epci_choisi %>% 
  merge(d_densite_resid_filter,
        by.x = "code_com",
        by.y = "code_com")  %>%
 affiche_carte_densite(titre = "Densit\u00e9 r\u00e9sidentielle des communes de l'EPCI",
                       titre_legende = "Logements/hectare",
                       labelsource = "Fichiers Foncier CEREMA")

```


