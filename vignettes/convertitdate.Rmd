---
title: "convertitdate"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{convertitdate}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(opm2020)
```

# Objectif
Convertir les dates d'un format character donné à un format date donné.  
Fonctionne aussi pour les vecteurs contenant plusieurs dates. Dans ce cas, __format_in__ et __format_out__ peuvent egalement prendre la forme d'un vecteur de format de dates.  
Si le __format_in__ passé en paramètre ne correspond pas au format d'entrée de la date, retourne *NA*.  
Si le __format_out__ ne correspond à aucun format de date, retourne __format_out__.

```{r}
ch_date1 <- "2014-01-30"
ch_date2 <- "2014-30-01"
v_ch_date3 <- c("2009-01-01", "02022010", "02-02-2010")
v_ch_date4 <- c("02/27/92", "02/27/92", "01/14/92", "02/28/92")

convertitdate(ch_date1, "%Y-%m-%d")
convertitdate(ch_date1, "%Y-%m-%d", "%B %d %Y")
convertitdate(ch_date2, "%Y-%d-%m", "%d/%m/%Y")
convertitdate(v_ch_date3, c("%Y-%d-%m", "%d%m%Y","%d-%m-%Y"), "%d/%m/%Y")
convertitdate(v_ch_date4, "%m/%d/%y", c("%d-%m-%Y", "%d %B %Y", "%d%b%y", "%Y%m%d"))
convertitdate(ch_date1, "%Y-%m-%D")
convertitdate(ch_date1, "%Y-%m-%d", "ymd")
```

